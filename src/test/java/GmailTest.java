import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailTest {

  @Test
  public void testOne() {
    System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    WebDriver driver = new ChromeDriver();
    driver.get("https://mail.google.com/");

    WebElement elementMail = driver.findElement(By.xpath("//input[@id='identifierId']"));
    elementMail.sendKeys("bogdan.sprysa@gmail.com");
    WebElement buttonNext = driver.findElement(By.id("identifierNext"));
    buttonNext.click();

    (new WebDriverWait(driver, 10))
        .until(ExpectedConditions.presenceOfElementLocated(By.id("passwordNext")));

    WebElement elementPassword = driver
        .findElement(By.cssSelector("input[type='password']:nth-child(1)"));
    elementPassword.sendKeys("*******");
    WebElement buttonPasswordNext = driver.findElement(By.id("passwordNext"));
    buttonPasswordNext.click();

    (new WebDriverWait(driver, 20))
        .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Написати']")));

    WebElement buttonCreateLetter = driver.findElement(By.xpath("//div[text()='Написати']"));
    buttonCreateLetter.click();

    (new WebDriverWait(driver, 20))
        .until(ExpectedConditions.presenceOfElementLocated(By.id(":py")));

    WebElement elementAdress = driver.findElement(By.id(":py"));
    elementAdress.sendKeys("sprysa@gmail.com");
    WebElement elementText = driver.findElement(By.id(":ql"));
    elementText.sendKeys("Hi from bogdan.sprysa@gmail.com !");

    WebElement buttonSend = driver.findElement(By.id(":p6"));
    buttonSend.click();

    try
    {
      Thread.sleep(5000);
    }
    catch(InterruptedException ex)
    {
      Thread.currentThread().interrupt();
    }
    driver.quit();
  }


}
